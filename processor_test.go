package goformal

import (
	"testing"
)

func makeSelector(rules map[string]string) Selector {
	return SelectorFunc(func(symbol string) Expander {
		rule, ok := rules[symbol]
		if !ok {
			return nil
		}

		return ExpanderFunc(func() string {
			return rule
		})
	})
}

func TestProcess(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		expanders Selector
		input     string
		expected  string
	}{
		{
			"ExpandSingleSymbol",
			makeSelector(map[string]string{
				"A": "alpha",
			}),
			"A",
			"alpha",
		},
		{
			"ExpandMultipleSymbols",
			makeSelector(map[string]string{
				"A": "alpha",
				"B": "beta",
			}),
			"A B",
			"alpha beta",
		},
		{
			"ExpandOnlyRegisteredSymbols",
			makeSelector(map[string]string{
				"A": "alpha",
				"B": "beta",
			}),
			"A hello B",
			"alpha hello beta",
		},
		{
			"PreservePunctuation",
			makeSelector(map[string]string{
				"A": "alpha",
			}),
			"A, A",
			"alpha, alpha",
		},
		{
			"ExpandSymbolsRecursively",
			makeSelector(map[string]string{
				"A": "B B",
				"B": "beta",
			}),
			"A",
			"beta beta",
		},
		{
			"ExpandWordSymbols",
			makeSelector(map[string]string{
				"ALPHA": "alpha",
			}),
			"ALPHA",
			"alpha",
		},
		{
			"DepthLimit",
			makeSelector(map[string]string{
				"A": "A",
			}),
			"A",
			"A",
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			p := NewProcessor(test.expanders)
			actual := p.Process(test.input)

			if actual != test.expected {
				t.Logf("expected: %q; actual: %q\n", test.expected, actual)
				t.FailNow()
			}
		})
	}
}
