package goformal

import (
	"strings"
	"unicode"
)

// Expander defines the method for expanding a symbol.
type Expander interface {
	Expand() string
}

// ExpanderFunc implements the Expander interface over a func.
type ExpanderFunc func() string

// Expand calls the underlying func.
func (fn ExpanderFunc) Expand() string {
	return fn()
}

// Selector defines the method for getting an Expander for a given
// symbol.
type Selector interface {
	Select(symbol string) Expander
}

// SelectorFunc implements the Selector interface over a func.
type SelectorFunc func(string) Expander

// Select calls the underlying func with the given symbol.
func (fn SelectorFunc) Select(symbol string) Expander {
	return fn(symbol)
}

// Processor takes a collection of rules and expands the symbols found inside
// until there are no more symbols.
type Processor struct {
	expanders  Selector
	depthLimit int
}

// NewProcessor instantiates a new Processor with a given Selector.
func NewProcessor(expanders Selector) *Processor {
	return &Processor{
		expanders:  expanders,
		depthLimit: 10,
	}
}

func isIdentifier(r rune) bool {
	return unicode.IsLetter(r) || unicode.IsNumber(r)
}

func tokenize(input string) []string {
	var fields []string

	var accum string
	for _, r := range []rune(input) {
		if len(accum) > 0 && isIdentifier(r) != isIdentifier(rune(accum[0])) {
			fields = append(fields, accum)
			accum = ""
		}

		accum += string(r)
	}
	if accum != "" {
		fields = append(fields, accum)
	}

	return fields
}

func (p Processor) process(input string, depth int) string {
	if depth >= p.depthLimit {
		return input
	}

	tokens := tokenize(input)
	for i := range tokens {
		if ex := p.expanders.Select(tokens[i]); ex != nil {
			tokens[i] = p.process(ex.Expand(), depth+1)
		}
	}
	return strings.Join(tokens, "")
}

// Process takes an input string and expands any symbols found recursively.
func (p Processor) Process(input string) string {
	return p.process(input, 0)
}
