VERSION := $(shell git describe --tags --always)

.PHONY: goformalc
goformalc:
	go build -ldflags '-X main.version=$(VERSION)' -o ./build/goformalc ./cmd/goformalc

.PHONY: goformalapi
goformalapi:
	go build -ldflags '-X main.version=$(VERSION)' -o ./build/goformalapi ./cmd/goformalapi

TESTCMD := go test
ifneq ($(shell which gotest),'')
	TESTCMD=$(shell which gotest)
endif

.PHONY: testcover
testcover:
	$(TESTCMD) -race -timeout 1s -coverprofile=coverage.out ./...
	go tool cover -html=coverage.out -o coverage.html
