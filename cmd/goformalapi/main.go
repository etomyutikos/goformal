package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"

	cli "github.com/jawher/mow.cli"

	"gitlab.com/etomyutikos/goformal"
	"gitlab.com/etomyutikos/goformal/cmd/internal/config"
)

var version string

func getFiles(globs []string) []io.Reader {
	var files []io.Reader
	for _, glob := range globs {
		paths, err := filepath.Glob(glob)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error finding filepaths from globs: %s\n", err)
			cli.Exit(1)
		}

		for _, path := range paths {
			file, err := os.Open(path)
			if err != nil {
				fmt.Fprintf(os.Stderr, "error opening file %q: %s\n", path, err)
				cli.Exit(1)
			}
			files = append(files, file)
		}
	}

	return files
}

func main() {
	app := cli.App("goformalapi", "an API for generating formal grammar nonsense")
	app.Version("version", version)

	var (
		port = app.Int(cli.IntOpt{
			Name:   "p port",
			Desc:   "port for the server to listen on",
			EnvVar: "GOFORMALAPI_PORT",
			Value:  8080,
		})
		ruleGlobs = app.Strings(cli.StringsOpt{
			Name:   "r rules",
			Desc:   "a file glob pointing to files containing JSON formatted rule definitions",
			EnvVar: "GOFORMALAPI_RULES",
			Value:  nil,
		})
	)
	app.Spec = "[-p] -r..."

	app.Action = func() {
		if ruleGlobs == nil || len(*ruleGlobs) == 0 {
			fmt.Fprint(os.Stderr, "must provide rule files")
			cli.Exit(1)
		}

		files := getFiles(*ruleGlobs)
		selector, err := config.FromJSON(files[0], files[1:]...)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to convert JSON rule files: %s\n", err)
			cli.Exit(1)
		}

		srv := http.Server{
			Addr:    fmt.Sprintf(":%d", *port),
			Handler: newHandler(version, goformal.NewProcessor(selector)),
		}

		stop := make(chan os.Signal)
		signal.Notify(stop, os.Interrupt, os.Kill)
		go func() {
			<-stop
			srv.Shutdown(context.Background())
		}()

		fmt.Fprintf(os.Stdout, "listening on port %d\n", *port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			fmt.Fprintf(os.Stderr, "unexpected error from ListenAndServe: %s", err)
			cli.Exit(1)
		}
	}

	app.Run(os.Args)
}
