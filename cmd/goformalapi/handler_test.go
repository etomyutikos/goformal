package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHandler(t *testing.T) {
	t.Parallel()

	t.Run("/version", func(t *testing.T) {
		t.Parallel()

		expected := "testtest"
		srv := httptest.NewServer(newHandler(expected, nil))
		defer srv.Close()

		r, err := http.Get(fmt.Sprintf("%s/version", srv.URL))
		require.NoError(t, err)
		defer r.Body.Close()

		actual, err := ioutil.ReadAll(r.Body)
		require.NoError(t, err)
		assert.Equal(t, fmt.Sprintf(`{"version":"%s"}`, expected), string(actual))
	})

	t.Run("/nonsense", func(t *testing.T) {
		t.Parallel()

		input := "A"
		expected := "hello, world!"
		p := processFunc(func(i string) string {
			require.Equal(t, input, i)
			return expected
		})

		srv := httptest.NewServer(newHandler("", p))
		defer srv.Close()

		r, err := http.Get(fmt.Sprintf("%s/nonsense?input=%s", srv.URL, input))
		require.NoError(t, err)
		defer r.Body.Close()

		var actual nonsenseResponse
		require.NoError(t, json.NewDecoder(r.Body).Decode(&actual))
		assert.Equal(t, expected, actual.Output)
	})
}
