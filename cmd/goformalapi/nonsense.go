package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type nonsenseResponse struct {
	Output string `json:"output"`
}

func getNonsense(processor processor) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		input := r.URL.Query().Get("input")
		if input == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("{\"error\":\"missing input query parameter\"}")))
			return
		}

		res := nonsenseResponse{
			Output: processor.Process(input),
		}
		if err := json.NewEncoder(w).Encode(res); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("{\"error\":\"%s\"}", err)))
		}
	}
}
