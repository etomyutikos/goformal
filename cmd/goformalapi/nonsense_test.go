package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type processFunc func(string) string

func (f processFunc) Process(input string) string {
	return f(input)
}

func Test_getNonsense(t *testing.T) {
	t.Parallel()

	input := "TEST"
	expected := "hello, world!"
	processor := processFunc(func(i string) string {
		require.Equal(t, input, i)
		return expected
	})

	handler := getNonsense(processor)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/nonsense?input=%s", input), nil)

	handler(w, r)
	assert.Equal(t, http.StatusOK, w.Code)
	require.Equal(t, fmt.Sprintf("{\"output\":\"%s\"}\n", expected), w.Body.String())
}
