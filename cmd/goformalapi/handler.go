package main

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type processor interface {
	Process(string) string
}

func newHandler(version string, processor processor) http.Handler {
	router := chi.NewRouter()
	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(middleware.NoCache)

	router.Get("/version", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(fmt.Sprintf("{\"version\":\"%s\"}", version)))
	})
	router.Get("/nonsense", getNonsense(processor))

	return router
}
