package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	cli "github.com/jawher/mow.cli"

	"gitlab.com/etomyutikos/goformal"
	"gitlab.com/etomyutikos/goformal/cmd/internal/config"
)

var version string

func getFiles(globs []string) []io.Reader {
	var files []io.Reader
	for _, glob := range globs {
		paths, err := filepath.Glob(glob)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error finding filepaths from globs: %s\n", err)
			cli.Exit(1)
		}

		for _, path := range paths {
			file, err := os.Open(path)
			if err != nil {
				fmt.Fprintf(os.Stderr, "error opening file %q: %s\n", path, err)
				cli.Exit(1)
			}
			files = append(files, file)
		}
	}

	return files
}

func main() {
	app := cli.App("goformalc", "a CLI for generating formal grammar nonsense")
	app.Version("version", version)

	var (
		ruleGlobs = app.Strings(cli.StringsOpt{
			Name:   "r rules",
			Desc:   "a file glob pointing to files containing JSON formatted rule definitions",
			EnvVar: "GOFORMALC_RULES",
			Value:  nil,
		})

		input = app.StringOpt("i input", "", "input string containing symbols to be expanded")
	)
	app.Spec = "-r... -i"

	app.Action = func() {
		if ruleGlobs == nil || len(*ruleGlobs) == 0 {
			fmt.Fprint(os.Stderr, "must provide rule files")
			cli.Exit(1)
		}

		if input == nil || *input == "" {
			fmt.Fprint(os.Stderr, "must provide input")
			cli.Exit(1)
		}

		files := getFiles(*ruleGlobs)
		selector, err := config.FromJSON(files[0], files[1:]...)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to convert JSON rule files: %s\n", err)
			cli.Exit(1)
		}

		fmt.Fprintf(os.Stdout, "%s\n",
			goformal.NewProcessor(selector).Process(*input),
		)
	}

	app.Run(os.Args)
}
