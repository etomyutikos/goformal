package config

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/etomyutikos/goformal"
	"gitlab.com/etomyutikos/goformal/expanders"
	"gitlab.com/etomyutikos/goformal/selectors"
)

// FromJSON takes JSON from io.Readers and converts it to a
// goformal.Selector. The output of multiple readers will be merged.
func FromJSON(first io.Reader, rest ...io.Reader) (goformal.Selector, error) {
	type symbol struct {
		Symbol    string `json:"symbol"`
		Expansion string `json:"expansion"`
	}
	var symbols []symbol

	rr := append([]io.Reader{first}, rest...)
	for _, r := range rr {
		var ss []symbol
		if err := json.NewDecoder(r).Decode(&ss); err != nil {
			return nil, fmt.Errorf("failed to decode symbols: %w", err)
		}
		symbols = append(symbols, ss...)
	}

	selector := make(selectors.Map)
	for _, symbol := range symbols {
		expander, ok := selector[symbol.Symbol]
		if !ok {
			expander = expanders.List{}
		}

		selector[symbol.Symbol] = append(expander.(expanders.List), symbol.Expansion)
	}

	return selector, nil
}
