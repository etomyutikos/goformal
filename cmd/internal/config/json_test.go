package config

import (
	"io"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/etomyutikos/goformal"
	"gitlab.com/etomyutikos/goformal/expanders"
	"gitlab.com/etomyutikos/goformal/selectors"
)

var symbolJSON = `
[
	{
		"symbol": "A",
		"expansion": "alpha"
	},
	{
		"symbol": "B",
		"expansion": "beta"
	},
	{
		"symbol": "B",
		"expansion": "bob"
	}
]
`

var symbolJSON1 = `
[
	{
		"symbol": "A",
		"expansion": "alpha"
	}
]`

var symbolJSON2 = `
[
	{
		"symbol": "B",
		"expansion": "beta"
	},
	{
		"symbol": "B",
		"expansion": "bob"
	}
]`

func assertSelector(t *testing.T, actual goformal.Selector) {
	t.Helper()

	selector, ok := actual.(selectors.Map)
	if !ok {
		t.Logf("expected: %s; actual: %s",
			reflect.TypeOf(selectors.Map{}).Name(),
			reflect.TypeOf(actual).Name(),
		)
		t.FailNow()
	}

	a := selector.Select("A")
	expander, ok := a.(expanders.List)
	if !ok {
		t.Logf("expected: %s; actual: %s",
			reflect.TypeOf(expanders.List{}).Name(),
			reflect.TypeOf(actual).Name(),
		)
		t.FailNow()
	}

	expansion := expander.Expand()
	if expansion != "alpha" {
		t.Logf("expected: %q; actual: %q", "alpha", expansion)
		t.FailNow()
	}

	b := selector.Select("B")
	expander, ok = b.(expanders.List)
	if !ok {
		t.Logf("expected: %s; actual: %s",
			reflect.TypeOf(expanders.List{}).Name(),
			reflect.TypeOf(actual).Name(),
		)
		t.FailNow()
	}

	if len(expander) != 2 {
		t.Logf("expected: %d; actual: %d", 2, len(expander))
		t.FailNow()
	}
	if expander[0] != "beta" {
		t.Logf("expected: %q; actual: %q", "beta", expander[0])
		t.FailNow()
	}
	if expander[1] != "bob" {
		t.Logf("expected: %q; actual: %q", "bob", expander[1])
		t.FailNow()
	}
}

func TestFromJSON(t *testing.T) {
	t.Parallel()

	t.Run("SingleReader", func(t *testing.T) {
		t.Parallel()

		r := strings.NewReader(symbolJSON)

		actual, err := FromJSON(r)
		if err != nil {
			t.Logf("unexpected error: %s", err)
			t.FailNow()
		}
		assertSelector(t, actual)
	})

	t.Run("MultipleReaders", func(t *testing.T) {
		t.Parallel()

		rr := []io.Reader{
			strings.NewReader(symbolJSON1),
			strings.NewReader(symbolJSON2),
		}

		actual, err := FromJSON(rr[0], rr[1:]...)
		if err != nil {
			t.Logf("unexpected error: %s", err)
			t.FailNow()
		}
		assertSelector(t, actual)
	})
}
