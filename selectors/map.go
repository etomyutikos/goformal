package selectors

import "gitlab.com/etomyutikos/goformal"

// Map implements the goformal.Selector interface over a map of
// string.
type Map map[string]goformal.Expander

// Select returns the goformal.Expander for the given symbol or nil if it's not
// in the map.
func (s Map) Select(symbol string) goformal.Expander {
	if e, ok := s[symbol]; ok {
		return e
	}
	return nil
}
