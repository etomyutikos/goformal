package selectors

import (
	"testing"

	"gitlab.com/etomyutikos/goformal"
)

func TestMapSelect(t *testing.T) {
	t.Parallel()

	t.Run("NonNil", func(t *testing.T) {
		t.Parallel()

		symbol := "A"
		expected := "hello, world!"
		s := Map(map[string]goformal.Expander{
			symbol: goformal.ExpanderFunc(func() string {
				return expected
			}),
		})

		ex := s.Select(symbol)
		if ex == nil {
			t.Log("unexpected nil")
			t.FailNow()
		}

		actual := ex.Expand()
		if actual != expected {
			t.Logf("expected %q; actual: %q", expected, actual)
			t.FailNow()
		}
	})

	t.Run("Nil", func(t *testing.T) {
		t.Parallel()

		symbol := "A"
		s := Map(map[string]goformal.Expander{})

		actual := s.Select(symbol)
		if actual != nil {
			t.Log("expected nil")
			t.FailNow()
		}
	})
}
