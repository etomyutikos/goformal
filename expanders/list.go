package expanders

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

// List implements the Expander interface over a slice of string.
type List []string

// Expand returns a randomly selected item from the underlying list.
func (e List) Expand() string {
	return e[rand.Intn(len(e))]
}
