package expanders

import "testing"

func TestStringExpand(t *testing.T) {
	t.Parallel()

	expected := "hello, world!"
	e := String(expected)

	actual := e.Expand()
	if actual != expected {
		t.Logf("expected: %q; actual: %q", expected, actual)
		t.FailNow()
	}
}
