package expanders

import "testing"

func TestListExpand(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		options []string
	}{
		{
			"Single",
			[]string{"alpha"},
		},
		{
			"Multiple",
			[]string{"alpha", "beta"},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			e := List(test.options)
			actual := e.Expand()

			var found bool
			for _, expected := range test.options {
				if actual == expected {
					found = true
					break
				}
			}
			if !found {
				t.Logf("expected one of: %v; actual: %s", test.options, actual)
			}
		})
	}
}
