package expanders

// String implements the Expander interface over a string.
type String string

// Expand returns the underlying string.
func (e String) Expand() string {
	return string(e)
}
