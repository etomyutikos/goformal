module gitlab.com/etomyutikos/goformal

go 1.15

require (
	github.com/go-chi/chi v1.5.0
	github.com/jawher/mow.cli v1.2.0
	github.com/stretchr/testify v1.6.1
)
